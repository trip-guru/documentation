.. gettripguru documentation master file, created by
   sphinx-quickstart on Sun Jun 27 18:06:11 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to gettripguru's documentation!
=======================================

.. toctree::
   :maxdepth: 2

   SourceCode
   Architecture
   Image Uploads
   PDF processor
   Homepage
   TripPlanner
   Authentication / Auth0
   Become a Guide
   Settings
   Localization
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
